#!/bin/bash

export CD_VER=`grep appVersion ./contdemo/gradle.properties | cut -f2 -d=`

export SDKMAN_DIR="/home/ibbo/.sdkman"
[[ -s "/home/ibbo/.sdkman/bin/sdkman-init.sh" ]] && source "/home/ibbo/.sdkman/bin/sdkman-init.sh"


sdk use grails 4.0.4
sdk use java 11.0.6.j9-adpt

cd contdemo

grails clean
grails prod war
cp build/libs/contdemo-$CD_VER.war ../docker/contdemo.war

cd ../docker

if [[ "$CD_VER" == *-SNAPSHOT ]]
then
  echo  SNAPSHOT release - only tagging :v$CD_VER and :latest
  docker build -t semweb/contdemo:v$CD_VER -t semweb/contdemo:latest .
  docker push semweb/contdemo:v$CD_VER
  docker push semweb/contdemo:latest
else
  echo  Standard Release v$CD_VER
  docker build -t semweb/contdemo:v$CD_VER -t semweb/contdemo:latest .
  docker push semweb/contdemo:v$CD_VER
  docker push semweb/contdemo:latest
fi

echo Completed release of contdemo $CD_VER
