


A trivial grails 4.0.4 application packaged up in such a way that when run with 

    java -jar contdemo.war 

The application will start in production mode using 

    jdbc:h2:./prodDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE

as the datasource URL but allowing a docker ENV to override this by setting the env var DB_URL. this happens by virtue of the config

    url: "${DB_URL:jdbc:h2:./prodDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE}"

in application.yml
