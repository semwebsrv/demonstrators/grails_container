package contdemo

class BootStrap {

  def grailsApplication

  def init = { servletContext ->
    println("Config");
    println("  -> ${grailsApplication.config.dataSource.url}");
  }

  def destroy = {
  }
}
